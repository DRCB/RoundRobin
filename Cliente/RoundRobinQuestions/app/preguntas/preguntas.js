'use strict';

angular.module('myApp.preguntas', ['ngRoute','ngAnimate','ngTouch','ui.bootstrap'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/preguntas', {
    templateUrl: 'preguntas/preguntas.html',
    controller: 'PreguntasController'
  });
}])

.factory('socket', ['$q', '$rootScope','Datos',function($q, $rootScope,Datos) {
  
  var callbacks = {};
  var callbackActual = 0;

  // Create our websocket object with the address to the websocket
  var ws = new WebSocket("ws://localhost:9001/");
  
  ws.onopen = function(){  
    console.log("Se ha establecido la conexion con el servidor");
  };
  
  ws.onmessage = function(mensaje) {
      //console.log(mensaje)
      listener(JSON.parse(mensaje.data));      
  };

  ws.onclose = function(){
    console.log("Se ha cerrado la conexion con el servidor");
  }

  ws.onerror = function(error) {
    console.log(error);     
  }; 

  function enviarMensaje(mensaje) {
    var defer = $q.defer();
    var callbackId = getCallbackId();
    callbacks[callbackId] = {
      tiempo: new Date(),
      callback:defer
    };
    mensaje.callback_id = callbackId;
    console.log('Enviando mensaje', mensaje);
    ws.send(JSON.stringify(mensaje));
    return defer.promise;
  }

  function listener(data) {
    var messageObj = data;
    console.log("Datos recibidos del servidor: ", messageObj);
    // If an object exists with callback_id in our callbacks object, resolve it
    if (messageObj.hasOwnProperty("callback_id")) {
      if(callbacks.hasOwnProperty(messageObj.callback_id)) {
        console.log(callbacks[messageObj.callback_id]);
        $rootScope.$apply(callbacks[messageObj.callback_id].callback.resolve(messageObj));
        delete callbacks[messageObj.callback_id];
      }
    }else if(messageObj.hasOwnProperty("tipo")){      
      if (messageObj.jugador == "procesador1"){
        if(messageObj.tipo == "tiempoL"){
          var scope = angular.element(document.getElementById('jugador1')).scope()
          scope.$apply(function(){
            var posProceso = scope.procesos1.map(function(proceso) { return proceso.nombre; }).indexOf(messageObj.pregunta);
            var posEstado = scope.procesos1[posProceso].lista.map(function(estado){return estado.tipo;}).lastIndexOf('success');
            scope.procesos1[posProceso].lista[posEstado].valor++;
            scope.procesos1[posProceso].tiempoTotal++;
            if(scope.procesos1[posProceso].tiempoTotal == scope.procesos1[posProceso].max){
             scope.procesos1[posProceso].max += 10
             scope.procesos1[posProceso].width += 30
            }            
          });
        }else if(messageObj.tipo == "critica"){
          var jugador1 = Datos.getJugador(1); 
          for (var i = 0; i < jugador1.preguntas.length; i++) {
            if (jugador1.preguntas[i].nombre === messageObj.pregunta) {                  
              var scope = angular.element(document.getElementById('jugador1')).scope()              
              scope.$apply(function(){
                 scope.jugador1 = jugador1.preguntas.splice(i,1)[0];
                 scope.listos1 = jugador1.preguntas
                 var posProceso = scope.procesos1.map(function(proceso) { return proceso.nombre; }).indexOf(messageObj.pregunta);
                 scope.procesos1[posProceso].lista.push({valor:0,tipo:'info'})
              });
            }
          }              
        }else if (messageObj.tipo == "tiempo"){
          var scope = angular.element(document.getElementById('jugador1')).scope()              
          scope.$apply(function(){
             scope.jugador1.tiempo = messageObj.tiempo;
             scope.jugador1.quantum = messageObj.quantum;
             var posProceso = scope.procesos1.map(function(proceso) { return proceso.nombre; }).indexOf(messageObj.pregunta);            
             var posEstado = scope.procesos1[posProceso].lista.map(function(estado){return estado.tipo;}).lastIndexOf('info');
             scope.procesos1[posProceso].lista[posEstado].valor++;
             scope.procesos1[posProceso].tiempoTotal++;
             if(scope.procesos1[posProceso].tiempoTotal == scope.procesos1[posProceso].max){
              scope.procesos1[posProceso].max += 10
              scope.procesos1[posProceso].width += 30
             }             
          });            
        }else if(messageObj.tipo == "suspendido"){
          var jugador1 = Datos.getJugador(1); 
          var scope = angular.element(document.getElementById('jugador1')).scope() 
          if(messageObj.entra){            
            scope.$apply(function(){
              jugador1.suspendidos.push(scope.jugador1)
              scope.jugador1 = {}
              scope.suspendidos1 = jugador1.suspendidos
              var posProceso = scope.procesos1.map(function(proceso) { return proceso.nombre; }).indexOf(messageObj.pregunta);
              scope.procesos1[posProceso].lista.push({valor:0,tipo:'warning'})
            });
          }else{
            for (var i = 0; i < jugador1.suspendidos.length; i++) {
              if (jugador1.suspendidos[i].nombre === messageObj.pregunta) {
                scope.$apply(function(){    
                  jugador1.suspendidos[i].penalizacion = 10              
                  jugador1.preguntas.push(jugador1.suspendidos.splice(i,1)[0]);
                  scope.listos1 = jugador1.preguntas
                  scope.suspendidos1 = jugador1.suspendidos  
                  var posProceso = scope.procesos1.map(function(proceso) { return proceso.nombre; }).indexOf(messageObj.pregunta);
                  scope.procesos1[posProceso].lista.push({valor:0,tipo:'success'})                                   
                });                
              }
            }
          }
        }else if(messageObj.tipo == "tiempoS"){
          var jugador1 = Datos.getJugador(1); 
          var scope = angular.element(document.getElementById('jugador1')).scope() 
          for (var i = 0; i < jugador1.suspendidos.length; i++) {
            if(jugador1.suspendidos[i].nombre == messageObj.pregunta){
              scope.$apply(function(){
                jugador1.suspendidos[i].penalizacion = messageObj.tiempo
                var posProceso = scope.procesos1.map(function(proceso) { return proceso.nombre; }).indexOf(messageObj.pregunta);
                var posEstado = scope.procesos1[posProceso].lista.map(function(estado){return estado.tipo;}).lastIndexOf('warning');
                scope.procesos1[posProceso].lista[posEstado].valor++;
                scope.procesos1[posProceso].tiempoTotal++;
                if(scope.procesos1[posProceso].tiempoTotal == scope.procesos1[posProceso].max){
                 scope.procesos1[posProceso].max += 10
                 scope.procesos1[posProceso].width += 30
                }
              });              
            }
          }
        }else if(messageObj.tipo == "bloqueado"){
          var jugador1 = Datos.getJugador(1); 
          var scope = angular.element(document.getElementById('jugador1')).scope() 
          if(messageObj.entra){
            for (var i = 0; i < jugador1.preguntas.length; i++) {
              if (jugador1.preguntas[i].nombre === messageObj.pregunta) {
                scope.$apply(function(){
                  jugador1.bloqueados.push(jugador1.preguntas.splice(i, 1)[0]);
                  scope.listos1 = jugador1.preguntas
                  scope.bloqueados1 = jugador1.bloqueados 
                  var posProceso = scope.procesos1.map(function(proceso) { return proceso.nombre; }).indexOf(messageObj.pregunta);
                  scope.procesos1[posProceso].lista.push({valor:0,tipo:'danger'})
                });                
              }
            }
          }else{
            for (var i = 0; i < jugador1.bloqueados.length; i++) {
              if (jugador1.bloqueados[i].nombre === messageObj.pregunta) {
                scope.$apply(function(){
                  jugador1.preguntas.push(jugador1.bloqueados.splice(i, 1)[0]); 
                  scope.listos1 = jugador1.preguntas
                  var posProceso = scope.procesos1.map(function(proceso) { return proceso.nombre; }).indexOf(messageObj.pregunta);
                  scope.procesos1[posProceso].lista.push({valor:0,tipo:'success'})
                });                
              }
            }
          }
        }else if(messageObj.tipo == "tiempoB"){
          var scope = angular.element(document.getElementById('jugador1')).scope()
          scope.$apply(function(){
            var posProceso = scope.procesos1.map(function(proceso) { return proceso.nombre; }).indexOf(messageObj.pregunta);
            var posEstado = scope.procesos1[posProceso].lista.map(function(estado){return estado.tipo;}).lastIndexOf('danger');
            scope.procesos1[posProceso].lista[posEstado].valor++;
            scope.procesos1[posProceso].tiempoTotal++;
            if(scope.procesos1[posProceso].tiempoTotal == scope.procesos1[posProceso].max){
             scope.procesos1[posProceso].max += 10
             scope.procesos1[posProceso].width += 30
            }
          });
        }else if(messageObj.tipo == "terminado"){
          var jugador1 = Datos.getJugador(1);
          var scope = angular.element(document.getElementById('jugador1')).scope()
          scope.$apply(function(){
            if(scope.jugador1.respuestaElegida == scope.jugador1.correcta){
              scope.puntaje1++;
            }
            jugador1.terminados.push(scope.jugador1)
            scope.jugador1 = {}
            scope.terminados1 = jugador1.terminados            
            var posProceso = scope.procesos1.map(function(proceso) { return proceso.nombre; }).indexOf(messageObj.pregunta);            
            scope.procesos1[posProceso].lista.push({valor:1,tipo:'terminado'})
          });          
        }
        // Datos.setJugador(1,jugador1);
      }else if (messageObj.jugador == "procesador2"){
        if(messageObj.tipo == "tiempoL"){
          var scope = angular.element(document.getElementById('jugador2')).scope()
          scope.$apply(function(){
            var posProceso = scope.procesos2.map(function(proceso) { return proceso.nombre; }).indexOf(messageObj.pregunta);
            var posEstado = scope.procesos2[posProceso].lista.map(function(estado){return estado.tipo;}).lastIndexOf('success');
            scope.procesos2[posProceso].lista[posEstado].valor++;
            scope.procesos2[posProceso].tiempoTotal++;
            if(scope.procesos2[posProceso].tiempoTotal == scope.procesos2[posProceso].max){
             scope.procesos2[posProceso].max += 10
             scope.procesos2[posProceso].width += 30
            }            
          });
        }else if(messageObj.tipo == "critica"){
          var jugador2 = Datos.getJugador(2); 
          for (var i = 0; i < jugador2.preguntas.length; i++) {
            if (jugador2.preguntas[i].nombre === messageObj.pregunta) {                  
              var scope = angular.element(document.getElementById('jugador2')).scope()              
              scope.$apply(function(){
                 scope.jugador2 = jugador2.preguntas.splice(i,1)[0];
                 scope.listos2 = jugador2.preguntas
                 var posProceso = scope.procesos2.map(function(proceso) { return proceso.nombre; }).indexOf(messageObj.pregunta);
                 scope.procesos2[posProceso].lista.push({valor:0,tipo:'info'})
              });
            }
          }              
        }else if(messageObj.tipo == "tiempo"){
          var scope = angular.element(document.getElementById('jugador2')).scope()              
          scope.$apply(function(){
             scope.jugador2.tiempo = messageObj.tiempo;
             scope.jugador2.quantum = messageObj.quantum;
             var posProceso = scope.procesos2.map(function(proceso) { return proceso.nombre; }).indexOf(messageObj.pregunta);            
             var posEstado = scope.procesos2[posProceso].lista.map(function(estado){return estado.tipo;}).lastIndexOf('info');
             scope.procesos2[posProceso].lista[posEstado].valor++;
             scope.procesos2[posProceso].tiempoTotal++;
             if(scope.procesos2[posProceso].tiempoTotal == scope.procesos2[posProceso].max){
              scope.procesos2[posProceso].max += 10
              scope.procesos2[posProceso].width += 30
             }
          });            
        }else if(messageObj.tipo == "suspendido"){
          var jugador2 = Datos.getJugador(2); 
          var scope = angular.element(document.getElementById('jugador2')).scope() 
          if(messageObj.entra){            
            scope.$apply(function(){
              jugador2.suspendidos.push(scope.jugador2)
              scope.jugador2 = {}
              scope.suspendidos2 = jugador2.suspendidos
              var posProceso = scope.procesos2.map(function(proceso) { return proceso.nombre; }).indexOf(messageObj.pregunta);
              scope.procesos2[posProceso].lista.push({valor:0,tipo:'warning'})
            });
          }else{
            for (var i = 0; i < jugador2.suspendidos.length; i++) {
              if (jugador2.suspendidos[i].nombre === messageObj.pregunta) {
                scope.$apply(function(){    
                  jugador2.suspendidos[i].penalizacion = 10              
                  jugador2.preguntas.push(jugador2.suspendidos.splice(i,1)[0]);
                  scope.listos2 = jugador2.preguntas
                  scope.suspendidos2 = jugador2.suspendidos 
                  var posProceso = scope.procesos2.map(function(proceso) { return proceso.nombre; }).indexOf(messageObj.pregunta);
                  scope.procesos2[posProceso].lista.push({valor:0,tipo:'success'})                                   
                });                
              }
            }
          }
        }else if(messageObj.tipo == "tiempoS"){
          var jugador2 = Datos.getJugador(2); 
          var scope = angular.element(document.getElementById('jugador2')).scope() 
          for (var i = 0; i < jugador2.suspendidos.length; i++) {
            if(jugador2.suspendidos[i].nombre == messageObj.pregunta){
              scope.$apply(function(){
                jugador2.suspendidos[i].penalizacion = messageObj.tiempo
                var posProceso = scope.procesos2.map(function(proceso) { return proceso.nombre; }).indexOf(messageObj.pregunta);
                var posEstado = scope.procesos2[posProceso].lista.map(function(estado){return estado.tipo;}).lastIndexOf('warning');
                scope.procesos2[posProceso].lista[posEstado].valor++;
                scope.procesos2[posProceso].tiempoTotal++;
                if(scope.procesos2[posProceso].tiempoTotal == scope.procesos2[posProceso].max){
                 scope.procesos2[posProceso].max += 10
                 scope.procesos2[posProceso].width += 30
                }
              });              
            }
          }
        }else if(messageObj.tipo == "bloqueado"){
          var jugador2 = Datos.getJugador(2);
          var scope = angular.element(document.getElementById('jugador2')).scope() 
          if(messageObj.entra){
            for (var i = 0; i < jugador2.preguntas.length; i++) {
              if (jugador2.preguntas[i].nombre === messageObj.pregunta) {                
                scope.$apply(function(){
                  jugador2.bloqueados.push(jugador2.preguntas.splice(i, 1)[0]); 
                  scope.listos2 = jugador2.preguntas
                  scope.bloqueados2 = jugador2.bloqueados
                  var posProceso = scope.procesos2.map(function(proceso) { return proceso.nombre; }).indexOf(messageObj.pregunta);
                  scope.procesos2[posProceso].lista.push({valor:0,tipo:'danger'})
                });
              }
            }
          }else{
            for (var i = 0; i < jugador2.bloqueados.length; i++) {
              if (jugador2.bloqueados[i].nombre === messageObj.pregunta) {
                scope.$apply(function(){
                  jugador2.preguntas.push(jugador2.bloqueados.splice(i, 1)[0]);
                  scope.listos2 = jugador2.preguntas 
                  var posProceso = scope.procesos2.map(function(proceso) { return proceso.nombre; }).indexOf(messageObj.pregunta);
                  scope.procesos2[posProceso].lista.push({valor:0,tipo:'success'})
                });                
              }
            }
          }
        }else if(messageObj.tipo == "tiempoB"){
          var scope = angular.element(document.getElementById('jugador2')).scope()
          scope.$apply(function(){
            var posProceso = scope.procesos2.map(function(proceso) { return proceso.nombre; }).indexOf(messageObj.pregunta);
            var posEstado = scope.procesos2[posProceso].lista.map(function(estado){return estado.tipo;}).lastIndexOf('danger');
            scope.procesos2[posProceso].lista[posEstado].valor++;
            scope.procesos2[posProceso].tiempoTotal++;
            if(scope.procesos2[posProceso].tiempoTotal == scope.procesos2[posProceso].max){
             scope.procesos2[posProceso].max += 10
             scope.procesos2[posProceso].width += 30
            }
          });
        }else if(messageObj.tipo == "terminado"){
          var jugador2 = Datos.getJugador(2);
          var scope = angular.element(document.getElementById('jugador2')).scope()
          scope.$apply(function(){
            if(scope.jugador2.respuestaElegida == scope.jugador2.correcta){
              scope.puntaje2++;
            }
            jugador2.terminados.push(scope.jugador2)
            scope.jugador2 = {}
            scope.terminados2 = jugador2.terminados
            var posProceso = scope.procesos2.map(function(proceso) { return proceso.nombre; }).indexOf(messageObj.pregunta);            
            scope.procesos2[posProceso].lista.push({valor:1,tipo:'terminado'})
          });
        }
        // Datos.setJugador(1,jugador1);      
      }else if (messageObj.jugador == "procesador3"){
        if(messageObj.tipo == "tiempoL"){
          var scope = angular.element(document.getElementById('jugador3')).scope()
          scope.$apply(function(){
            var posProceso = scope.procesos3.map(function(proceso) { return proceso.nombre; }).indexOf(messageObj.pregunta);
            var posEstado = scope.procesos3[posProceso].lista.map(function(estado){return estado.tipo;}).lastIndexOf('success');
            scope.procesos3[posProceso].lista[posEstado].valor++;
            scope.procesos3[posProceso].tiempoTotal++;
            if(scope.procesos3[posProceso].tiempoTotal == scope.procesos3[posProceso].max){
             scope.procesos3[posProceso].max += 10
             scope.procesos3[posProceso].width += 30
            }            
          });
        }else if(messageObj.tipo == "critica"){
          var jugador3 = Datos.getJugador(3); 
          for (var i = 0; i < jugador3.preguntas.length; i++) {
            if (jugador3.preguntas[i].nombre === messageObj.pregunta) {                  
              var scope = angular.element(document.getElementById('jugador3')).scope()              
              scope.$apply(function(){
                 scope.jugador3 = jugador3.preguntas.splice(i,1)[0];
                 scope.listos3 = jugador3.preguntas
                 var posProceso = scope.procesos3.map(function(proceso) { return proceso.nombre; }).indexOf(messageObj.pregunta);
                 scope.procesos3[posProceso].lista.push({valor:0,tipo:'info'})
              });
            }
          }              
        }else if (messageObj.tipo == "tiempo"){
          var scope = angular.element(document.getElementById('jugador3')).scope()              
          scope.$apply(function(){
             scope.jugador3.tiempo = messageObj.tiempo;
             scope.jugador3.quantum = messageObj.quantum;
             var posProceso = scope.procesos3.map(function(proceso) { return proceso.nombre; }).indexOf(messageObj.pregunta);            
             var posEstado = scope.procesos3[posProceso].lista.map(function(estado){return estado.tipo;}).lastIndexOf('info');
             scope.procesos3[posProceso].lista[posEstado].valor++;
             scope.procesos3[posProceso].tiempoTotal++;
             if(scope.procesos3[posProceso].tiempoTotal == scope.procesos3[posProceso].max){
              scope.procesos3[posProceso].max += 10
              scope.procesos3[posProceso].width += 30
             }
          });            
        }else if(messageObj.tipo == "suspendido"){
          var jugador3 = Datos.getJugador(3); 
          var scope = angular.element(document.getElementById('jugador3')).scope() 
          if(messageObj.entra){            
            scope.$apply(function(){
              jugador3.suspendidos.push(scope.jugador3)
              scope.jugador3 = {}
              scope.suspendidos3 = jugador3.suspendidos
              var posProceso = scope.procesos3.map(function(proceso) { return proceso.nombre; }).indexOf(messageObj.pregunta);
              scope.procesos3[posProceso].lista.push({valor:0,tipo:'warning'})
            });
          }else{
            for (var i = 0; i < jugador3.suspendidos.length; i++) {
              if (jugador3.suspendidos[i].nombre === messageObj.pregunta) {
                scope.$apply(function(){    
                  jugador3.suspendidos[i].penalizacion = 10              
                  jugador3.preguntas.push(jugador3.suspendidos.splice(i,1)[0]);
                  scope.listos3 = jugador3.preguntas
                  scope.suspendidos3 = jugador3.suspendidos
                  var posProceso = scope.procesos3.map(function(proceso) { return proceso.nombre; }).indexOf(messageObj.pregunta);
                  scope.procesos3[posProceso].lista.push({valor:0,tipo:'success'})                                    
                });                
              }
            }
          }
        }else if(messageObj.tipo == "tiempoS"){
          var jugador3 = Datos.getJugador(3); 
          var scope = angular.element(document.getElementById('jugador3')).scope() 
          for (var i = 0; i < jugador3.suspendidos.length; i++) {
            if(jugador3.suspendidos[i].nombre == messageObj.pregunta){
              scope.$apply(function(){
                jugador3.suspendidos[i].penalizacion = messageObj.tiempo
                var posProceso = scope.procesos3.map(function(proceso) { return proceso.nombre; }).indexOf(messageObj.pregunta);
                var posEstado = scope.procesos3[posProceso].lista.map(function(estado){return estado.tipo;}).lastIndexOf('warning');
                scope.procesos3[posProceso].lista[posEstado].valor++;
                scope.procesos3[posProceso].tiempoTotal++;
                if(scope.procesos3[posProceso].tiempoTotal == scope.procesos3[posProceso].max){
                 scope.procesos3[posProceso].max += 10
                 scope.procesos3[posProceso].width += 30
                }
              });              
            }
          }
        }else if(messageObj.tipo == "bloqueado"){
          var jugador3 = Datos.getJugador(3);
          var scope = angular.element(document.getElementById('jugador3')).scope()
          if(messageObj.entra){
            for (var i = 0; i < jugador3.preguntas.length; i++) {
              if (jugador3.preguntas[i].nombre === messageObj.pregunta) {
                scope.$apply(function(){
                  jugador3.bloqueados.push(jugador3.preguntas.splice(i, 1)[0]);
                  scope.listos3 = jugador3.preguntas
                  scope.bloqueados3 = jugador3.bloqueados 
                  var posProceso = scope.procesos3.map(function(proceso) { return proceso.nombre; }).indexOf(messageObj.pregunta);
                  scope.procesos3[posProceso].lista.push({valor:0,tipo:'danger'})
                });                
              }
            }
          }else{
            for (var i = 0; i < jugador3.bloqueados.length; i++) {
              if (jugador3.bloqueados[i].nombre === messageObj.pregunta) {
                scope.$apply(function(){
                  jugador3.preguntas.push(jugador3.bloqueados.splice(i, 1)[0]); 
                  scope.listos3 = jugador3.preguntas
                  var posProceso = scope.procesos3.map(function(proceso) { return proceso.nombre; }).indexOf(messageObj.pregunta);
                  scope.procesos3[posProceso].lista.push({valor:0,tipo:'success'})
                });                
              }
            }
          }
        }else if(messageObj.tipo == "tiempoB"){
          var scope = angular.element(document.getElementById('jugador3')).scope()
          scope.$apply(function(){
            var posProceso = scope.procesos3.map(function(proceso) { return proceso.nombre; }).indexOf(messageObj.pregunta);
            var posEstado = scope.procesos3[posProceso].lista.map(function(estado){return estado.tipo;}).lastIndexOf('danger');
            scope.procesos3[posProceso].lista[posEstado].valor++;
            scope.procesos3[posProceso].tiempoTotal++;
            if(scope.procesos3[posProceso].tiempoTotal == scope.procesos3[posProceso].max){
             scope.procesos3[posProceso].max += 10
             scope.procesos3[posProceso].width += 30
            }
          });
        }else if(messageObj.tipo == "terminado"){
          var jugador3 = Datos.getJugador(3);
          var scope = angular.element(document.getElementById('jugador3')).scope()
          scope.$apply(function(){
            if(scope.jugador3.respuestaElegida == scope.jugador3.correcta){
              scope.puntaje3++;
            }
            jugador3.terminados.push(scope.jugador3)
            scope.jugador3 = {}
            scope.terminados3 = jugador3.terminados
            var posProceso = scope.procesos3.map(function(proceso) { return proceso.nombre; }).indexOf(messageObj.pregunta);            
            scope.procesos3[posProceso].lista.push({valor:1,tipo:'terminado'})
          });          
        }
        // Datos.setJugador(1,jugador1);      
      }      
    }    
  }
  // This creates a new callback ID for a request
  function getCallbackId() {
    callbackActual += 1;
    if(callbackActual > 10000) {
      callbackActual = 0;
    }
    return callbackActual;
  }

  return {
    enviarMensaje: enviarMensaje,
    cerrar: function(){
      ws.close()
    }
  };
}])
.factory('Datos',[function(){
  var categorias = ["Matematicas","Ciencias Sociales","Biologia","Economia"];
  var jugador1 = {preguntas:[],suspendidos:[],bloqueados:[],terminados:[]};
  var jugador2 = {preguntas:[],suspendidos:[],bloqueados:[],terminados:[]};
  var jugador3 = {preguntas:[],suspendidos:[],bloqueados:[],terminados:[]};
  var jugadores = ["Jugador 1","Jugador 2","Jugador 3"];
  var preguntaActual = 0;
  
  function agregarPregunta(nuevaPregunta){
    if (nuevaPregunta.jugador == "Jugador 1"){      
      jugador1.preguntas.push(nuevaPregunta);                  
      console.log(jugador1);
    }else if (nuevaPregunta.jugador == "Jugador 2"){
      jugador2.preguntas.push(nuevaPregunta);      
      console.log(jugador2);
    }else {
      jugador3.preguntas.push(nuevaPregunta);
      console.log(jugador3);
    }
  }

  function agregarCategoria(nuevaCategoria){
    if (categorias.indexOf(nuevaCategoria) != -1){
      alert("Esta categoria ya existe");   
      return false;   
    }else{
      categorias.push(nuevaCategoria)      
      return true;
    }
  }

  function getPreguntaID(){
    preguntaActual++;
    return preguntaActual
  }

  function getJugador(indice){
    if (indice == 1){
      return jugador1
    }else if (indice == 2){
      return jugador2
    }else if (indice == 3){
      return jugador3
    }
  }

  function setJugador(indice,jugador){
    if(indice == 1){
      jugador1 = jugador
    }else if (indice == 2){
      jugador2 = jugador
    }else if(indice == 3){
      jugador3 = jugador
    }
  }

  return {
    categorias:categorias,
    jugadores:jugadores,
    agregarPregunta:agregarPregunta,
    agregarCategoria:agregarCategoria,
    getPreguntaID:getPreguntaID,
    getJugador: getJugador,
    setJugador: setJugador
  };

}])

.controller('PreguntasController', ['$scope','$uibModal','socket','Datos',function($scope,$uibModal,socket,Datos) {
  $scope.estaPausado = false;  
  $scope.procesos1 = []
  $scope.procesos2 = []
  $scope.procesos3 = []
  $scope.listos1 = []
  $scope.listos2 = []
  $scope.listos3 = []
  $scope.puntaje1 = 0
  $scope.puntaje2 = 0
  $scope.puntaje3 = 0
  $scope.abrirModalPreguntas = function(){
    $scope.pausar()
    var modalPreguntas = $uibModal.open({     
      animation: true,   
      templateUrl: 'modalPreguntas.html',
      controller: 'ModalController'      
    });

    modalPreguntas.result.then(function(nuevaPregunta){
      var objetoMensaje = {
        tipo: "tiempo",                
        procesador:nuevaPregunta.jugador
      } 
      socket.enviarMensaje(objetoMensaje).then(function(data){
        console.log(data);        
        var objetoGantt = {
          nombre: nuevaPregunta.nombre,
          width: 200,
          max:25,
          tiempoTotal:data.respuesta,
          lista:[{valor:data.respuesta,tipo:'servidor'},{valor:0,tipo:'success'}] //'success', 'info', 'warning', 'danger'
        }
        if (data.respuesta >= 25){
          objetoGantt.max = Math.round(data.respuesta + ((data.respuesta/25)*10))
          objetoGantt.width = Math.round(200 + ((data.respuesta/25)*30))
        }
        if (nuevaPregunta.jugador == "Jugador 1"){
          $scope.listos1 = Datos.getJugador(1).preguntas     
          $scope.procesos1.push(objetoGantt)
        }else if (nuevaPregunta.jugador == "Jugador 2"){
          $scope.listos2 = Datos.getJugador(2).preguntas
          $scope.procesos2.push(objetoGantt)
        }else {
          $scope.listos3 = Datos.getJugador(3).preguntas          
          $scope.procesos3.push(objetoGantt)
        } 
      });          
    });
  }

  $scope.abrirModalCategorias = function(){
    $scope.pausar()
    $uibModal.open({     
      animation: true,   
      templateUrl: 'modalCategorias.html',
      controller: 'ModalController'      
    });
  }

  $scope.abrirModalEscala = function(){
    $scope.pausar()
    $uibModal.open({
      animation: true,
      templateUrl: 'modalEscala.html',
      controller:'ModalController'
    });
  }

  $scope.pausar = function(){
    if(!$scope.estaPausado){
      var mensajePausa = {
        tipo:"pausar"
      }
      socket.enviarMensaje(mensajePausa).then(function(data){
        console.log(data)
        $scope.estaPausado = true
      });
    }    
  }

  $scope.resumir = function(){
    if($scope.estaPausado){
      var mensajeResumir = {
        tipo:"resumir"
      }
      socket.enviarMensaje(mensajeResumir).then(function(data){
        console.log(data)
        $scope.estaPausado = false
      });
    }    
  }
  
  $scope.responder1 = function(){
    $scope.jugador1.respuestaElegida = $scope.respuestas.jugador1
    var objetoMensaje = {
      tipo:"expulsion",
      procesador: "1"
    }
    socket.enviarMensaje(objetoMensaje).then(function(data){
      console.log(data)
    })
  }

  $scope.responder2 = function(){
    $scope.jugador2.respuestaElegida = $scope.respuestas.jugador2
    var objetoMensaje = {
      tipo:"expulsion",
      procesador: "2"
    }
    socket.enviarMensaje(objetoMensaje).then(function(data){
      console.log(data)
    })
  }

  $scope.responder3 = function(){
    $scope.jugador3.respuestaElegida = $scope.respuestas.jugador3
    var objetoMensaje = {
      tipo:"expulsion",
      procesador: "3"
    }
    socket.enviarMensaje(objetoMensaje).then(function(data){
      console.log(data)
    })
  }

  // $scope.onCloseClick = function() {
  //   socket.cerrar();
  // }

}])
.controller("ModalController",['$scope','$uibModalInstance','Datos','socket',function($scope,$uibModalInstance,Datos,socket){
  $scope.categorias = Datos.categorias;
  $scope.jugadores = Datos.jugadores;  
  
  $scope.crearPregunta = function(){
    $scope.nuevaPregunta.nombre = "pregunta"+Datos.getPreguntaID()
    $scope.nuevaPregunta.penalizacion = 10
    Datos.agregarPregunta($scope.nuevaPregunta);
    $uibModalInstance.close($scope.nuevaPregunta);
    var objetoMensaje = {
      tipo: "proceso",
      nombre: $scope.nuevaPregunta.nombre,
      tiempo:$scope.nuevaPregunta.tiempo,
      recurso:$scope.nuevaPregunta.categoria,
      procesador:$scope.nuevaPregunta.jugador
    } 
    socket.enviarMensaje(objetoMensaje).then(function(data){
      console.log(data);
    });
  }

  $scope.cancelar = function () {
    $uibModalInstance.dismiss();
  }

  $scope.crearCategoria = function(){
    var enviarMensaje = Datos.agregarCategoria($scope.nuevaCategoria.nombre);
    $uibModalInstance.close();
    if (enviarMensaje){
      var objetoMensaje = {
        tipo: "recurso",
        nombre: $scope.nuevaCategoria.nombre
      } 
      socket.enviarMensaje(objetoMensaje).then(function(data){
        console.log(data);
      });
    }
  }

  $scope.cambiarEscala = function(){
    $uibModalInstance.close();
    var objetoMensaje = {
      tipo: "escala",              
      escala:$scope.nuevaEscala.escala
    } 
    socket.enviarMensaje(objetoMensaje).then(function(data){
      console.log(data)      
    });    
  }

}]);