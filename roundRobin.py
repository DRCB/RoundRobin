from cola import Cola
import threading
import time
import Queue
import math
import json

class Procesador(threading.Thread):
    def __init__(self,nombre,colaSincronizacion):
        threading.Thread.__init__(self)
        self.name = nombre
        self.procesando = False
        self.__escala = 1
        self.__tiempo = 0
        self.__expulsion = False
        self.__pausado = False
        self.__condicion_pausa = threading.Condition(threading.RLock())
        self.__colaSincronizacion = colaSincronizacion
        self.procesoAsignado = None
        self.__colaListos = Cola()
        self.__colaSuspendidos = Cola()
        self.__colaBloqueados = Cola()
        self.__colaTerminados = Cola()
        self.__servidor = None
        self.__cliente = None

    def agregarProceso(self,proceso):
        self.__colaSincronizacion.put(proceso)
        print "\n",self.name,": ","SE HA AGREGADO UN NUEVO PROCESO"
    
    def setConexion(self,servidor,cliente):
        self.__servidor = servidor
        self.__cliente = cliente
    
    def pausar(self):
        self.__pausado = True        
        self.__condicion_pausa.acquire()
    
    def resumir(self):
        self.__pausado = False        
        self.__condicion_pausa.notify()        
        self.__condicion_pausa.release()
    
    def getTiempo(self):
        return self.__tiempo

    def setEscala(self,escala):
        self.__escala = escala
    
    def setExpulsion(self,expulsion):
        self.__expulsion = expulsion

    # def asignarQuantum(self):
    #     tamanio = self.__colaListos.tamanio
    #     tiempoTotal = 0
    #     for i in range(0,tamanio):
    #         proceso = self.__colaListos.pop()
    #         tiempoTotal += proceso.tiempoEjecucion
    #         self.__colaListos.push(proceso)
    #     promedio = tiempoTotal/tamanio
    #     quantum = promedio *0.8
        
    #     return quantum

        
    def run(self):
        while True:
            with self.__condicion_pausa:
                while self.__pausado:
                    self.__condicion_pausa.wait()
                        
                while not self.__colaSincronizacion.empty():           
                    proceso = self.__colaSincronizacion.get()
                    tamanio = self.__colaListos.tamanio
                    tiempoTotal = 0
                    for i in range(0,tamanio):
                        proceso = self.__colaListos.pop()
                        tiempoTotal += proceso.tiempoEjecucion
                        self.__colaListos.push(proceso)

                    proceso.asignarQuantum(tiempoTotal,tamanio)
                    self.__colaListos.push(proceso)
                
                self.conteoColaListos()

                if not self.__colaListos.vacia() and self.procesando==False:
                    proceso = self.__colaListos.pop()
                    if proceso.getRecurso().estaEnUso():
                        print "\nEl ",proceso.getRecurso().nombre," esta en uso"
                        print "\nEl ",proceso.nombre," entra a bloqueado"
                        self.__colaBloqueados.push(proceso)
                        mensaje = json.JSONEncoder().encode({"tipo":"bloqueado","jugador":self.name,"pregunta":proceso.nombre,"entra":True})
                        self.__servidor.send_message(self.__cliente,mensaje)
                    else:
                        self.procesando = True
                        proceso.getRecurso().setEnUso(True)
                        print "\nEl ",proceso.getRecurso().nombre," se empieza a usar por el ", proceso.nombre
                        self.procesoAsignado = proceso
                
                self.actualizarBloqueados()
                
                if self.procesando:
                    self.seccionCritica(self.procesoAsignado)
                else:
                    self.aplicarPenalizacion()
                    
                self.__tiempo += 1
                time.sleep(1 * self.__escala)


    def seccionCritica(self,proceso):          
        self.__expulsion = False
        if proceso.tiempoEjecucion > proceso.quantum:
            mensaje = json.JSONEncoder().encode({"tipo":"critica","jugador":self.name,"pregunta":proceso.nombre,"entra":True})
            self.__servidor.send_message(self.__cliente,mensaje)
            for i in range(0,proceso.quantum):
                with self.__condicion_pausa:
                    while self.__pausado:
                        self.__condicion_pausa.wait()
                    proceso.tiempoEjecucion -= 1
                    proceso.tiempoSeccionCritica += 1
                    print "\n",self.name,": ","Tiempo de ejecucion del ",proceso.nombre,": ", proceso.tiempoEjecucion
                    mensaje = json.JSONEncoder().encode({"tipo":"tiempo","jugador":self.name,"pregunta":proceso.nombre,"tiempo":proceso.tiempoEjecucion,"quantum":proceso.quantum})
                    self.__servidor.send_message(self.__cliente,mensaje)
                    self.aplicarPenalizacion()
                    self.__tiempo += 1
                    time.sleep(1 * self.__escala)
                    if self.__expulsion:
                        proceso.tiempoEjecucion = 0
                        break

            if self.__expulsion:
                proceso.getRecurso().setEnUso(False)
                print "\nEl ",proceso.getRecurso().nombre," se deja de usar por el ", proceso.nombre
                print "\n",self.name,": ","El ",proceso.nombre," Entra a Terminado"
                self.__colaTerminados.push(proceso)
                mensaje = json.JSONEncoder().encode({"tipo":"terminado","jugador":self.name,"pregunta":proceso.nombre,"entra":True})
                self.__servidor.send_message(self.__cliente,mensaje)  
                self.procesoAsignado = None
                self.__colaSincronizacion.task_done()
                
            else:            
                proceso.getRecurso().setEnUso(False)
                print "\nEl ",proceso.getRecurso().nombre," se deja de usar por el ", proceso.nombre            
                self.__colaSuspendidos.push(proceso)
                self.procesoAsignado = None
                print "\n",self.name,": ","El ",proceso.nombre," Entra a Suspendido"    
                mensaje = json.JSONEncoder().encode({"tipo":"suspendido","jugador":self.name,"pregunta":proceso.nombre,"entra":True})
                self.__servidor.send_message(self.__cliente,mensaje)        
        else:
            mensaje = json.JSONEncoder().encode({"tipo":"critica","jugador":self.name,"pregunta":proceso.nombre,"entra":True})
            self.__servidor.send_message(self.__cliente,mensaje)
            for i in range(0,proceso.tiempoEjecucion):
                with self.__condicion_pausa:
                    while self.__pausado:
                        self.__condicion_pausa.wait()
                    proceso.tiempoEjecucion -= 1
                    proceso.tiempoSeccionCritica += 1
                    print "\n",self.name,": ","Tiempo de ejecucion del ",proceso.nombre,": ", proceso.tiempoEjecucion
                    mensaje = json.JSONEncoder().encode({"tipo":"tiempo","jugador":self.name,"pregunta":proceso.nombre,"tiempo":proceso.tiempoEjecucion,"quantum":proceso.quantum})
                    self.__servidor.send_message(self.__cliente,mensaje)
                    self.aplicarPenalizacion()
                    self.__tiempo += 1
                    time.sleep(1 * self.__escala)
                    if self.__expulsion:
                        proceso.tiempoEjecucion = 0
                        break
            
            proceso.getRecurso().setEnUso(False)
            print "\nEl ",proceso.getRecurso().nombre," se deja de usar por el ", proceso.nombre
            print "\n",self.name,": ","El ",proceso.nombre," Entra a Terminado"
            self.__colaTerminados.push(proceso)
            mensaje = json.JSONEncoder().encode({"tipo":"terminado","jugador":self.name,"pregunta":proceso.nombre,"entra":True})
            self.__servidor.send_message(self.__cliente,mensaje)  
            self.procesoAsignado = None
            self.__colaSincronizacion.task_done() 

        self.procesando = False    
        
    
    def aplicarPenalizacion(self):
        tamanio = self.__colaSuspendidos.tamanio
        for i in range(0,tamanio):
            with self.__condicion_pausa:
                while self.__pausado:
                    self.__condicion_pausa.wait()            
                suspendido = self.__colaSuspendidos.pop()
                if suspendido.penalizacion == 0:
                    suspendido.penalizacion = 10  
                    tamanio = self.__colaListos.tamanio
                    tiempoTotal = 0
                    for i in range(0,tamanio):
                        proceso = self.__colaListos.pop()
                        tiempoTotal += proceso.tiempoEjecucion
                        self.__colaListos.push(proceso)              
                    suspendido.asignarQuantum(tiempoTotal,tamanio)
                    self.__colaListos.push(suspendido)
                    print "\nEL proceso",suspendido.nombre,"Sale de suspendio y entra a listo"
                    mensaje = json.JSONEncoder().encode({"tipo":"suspendido","jugador":self.name,"pregunta":suspendido.nombre,"entra":False})
                    self.__servidor.send_message(self.__cliente,mensaje)
                else:
                    suspendido.penalizacion -= 1
                    suspendido.tiempoSuspendido += 1
                    self.__colaSuspendidos.push(suspendido)
                    mensaje = json.JSONEncoder().encode({"tipo":"tiempoS","jugador":self.name,"pregunta":suspendido.nombre,"tiempo":suspendido.penalizacion})
                    self.__servidor.send_message(self.__cliente,mensaje)
                
    
    def actualizarBloqueados(self):
        tamanio = self.__colaBloqueados.tamanio
        for i in range(0,tamanio):  
            with self.__condicion_pausa:
                while self.__pausado:
                    self.__condicion_pausa.wait()          
                bloqueado = self.__colaBloqueados.pop()
                print "\nEl recurso",bloqueado.getRecurso().nombre,"sigue en uso? ", bloqueado.getRecurso().estaEnUso()
                if not bloqueado.getRecurso().estaEnUso():
                    tamanio = self.__colaListos.tamanio
                    tiempoTotal = 0
                    for i in range(0,tamanio):
                        proceso = self.__colaListos.pop()
                        tiempoTotal += proceso.tiempoEjecucion
                        self.__colaListos.push(proceso)
                    bloqueado.asignarQuantum(tiempoTotal,tamanio)
                    self.__colaListos.push(bloqueado)
                    print "\nEl proceso",bloqueado.nombre,"sale de bloqueado y entra a listo"
                    mensaje = json.JSONEncoder().encode({"tipo":"bloqueado","jugador":self.name,"pregunta":bloqueado.nombre,"entra":False})
                    self.__servidor.send_message(self.__cliente,mensaje)
                else:
                    bloqueado.tiempoBloqueado += 1
                    self.__colaBloqueados.push(bloqueado) 
                    mensaje = json.JSONEncoder().encode({"tipo":"tiempoB","jugador":self.name,"pregunta":bloqueado.nombre})
                    self.__servidor.send_message(self.__cliente,mensaje)                
    
    def conteoColaListos(self):
        tamanio = self.__colaListos.tamanio
        for i in range(0,tamanio):
            with self.__condicion_pausa:
                while self.__pausado:
                    self.__condicion_pausa.wait()
                listo = self.__colaListos.pop()
                listo.tiempoListo += 1
                self.__colaListos.push(listo)
                mensaje = json.JSONEncoder().encode({"tipo":"tiempoL","jugador":self.name,"pregunta":listo.nombre})
                self.__servidor.send_message(self.__cliente,mensaje)            

class Proceso:    
    def __init__(self,nombre,tiempoEjecucion,recurso):
        self.nombre = nombre
        self.quantum = None
        self.tiempoEjecucion = tiempoEjecucion
        self.__recurso = recurso
        self.tiempoListo = 0
        self.tiempoSuspendido = 0
        self.tiempoBloqueado = 0
        self.tiempoSeccionCritica = 0
        self.penalizacion = 10
       
    def getRecurso(self):
        return self.__recurso
    
    def asignarQuantum(self,tiempoTotal,tamanio):        
        # if self.tiempoEjecucion <= 5:
        #     self.quantum = self.tiempoEjecucion
        # elif self.tiempoEjecucion <= 15:
        #     aux = self.tiempoEjecucion/3
        #     self.quantum = int(math.floor(aux))
        # elif self.tiempoEjecucion <= 25:
        #     aux = self.tiempoEjecucion/5
        #     self.quantum = int(math.floor(aux))
        # else:
        #     aux = self.tiempoEjecucion/7
        #     self.quantum = int(math.floor(aux))
        if tiempoTotal > 0:
            promedio = tiempoTotal/tamanio
            quantum = promedio *0.8
            self.quantum = quantum
        else:
            if self.tiempoEjecucion <= 5:
                self.quantum = self.tiempoEjecucion
            elif self.tiempoEjecucion <= 15:
                aux = self.tiempoEjecucion/3
                self.quantum = int(math.floor(aux))
            elif self.tiempoEjecucion <= 25:
                aux = self.tiempoEjecucion/5
                self.quantum = int(math.floor(aux))
            else:
                aux = self.tiempoEjecucion/7
                self.quantum = int(math.floor(aux))
        

class Recurso:
    def __init__(self, nombre):
        self.nombre = nombre
        self.__enUso = False

    def estaEnUso(self):
        return self.__enUso        
    
    def setEnUso(self,enUso):
            self.__enUso = enUso

            
    
# -----Main para pruebas-----

# recurso1 = Recurso("recurso1")
# recurso2 = Recurso("recurso2")
# recurso3 = Recurso("recurso3")
# recurso4 = Recurso("recurso4")
# colaSincronizacionProcesador1 = Queue.Queue()
# colaSincronizacionProcesador2 = Queue.Queue()
# #colaSincronizacionProcesador3 = Queue.Queue()
# procesador1 = Procesador("Procesador1",colaSincronizacionProcesador1)
# procesador2 = Procesador("Procesador2",colaSincronizacionProcesador2)
# procesador3 = Procesador("Procesador3",colaSincronizacionProcesador3)
# procesador1.start()
# procesador2.start()
# procesador3.start()

# proceso1 = Proceso("proceso1",6,recurso1)
# proceso2 = Proceso("proceso2",5,recurso3)
# proceso3 = Proceso("proceso3",5,recurso1)
# proceso4 = Proceso("proceso4",3,recurso3)
# proceso5 = Proceso("proceso5",7,recurso1)
# proceso6 = Proceso("proceso6",4,recurso4)
# procesador1.agregarProceso(proceso1)
# procesador2.agregarProceso(proceso5)
# time.sleep(2)
# procesador2.agregarProceso(proceso4)
# procesador1.agregarProceso(proceso3)
# procesador3.agregarProceso(proceso2)
# procesador3.agregarProceso(proceso6)
